$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 3,
                nav: true,
                loop: true,
                margin: 20
            },
            1400: {
                items: 5,
                nav: true,
                loop: true,
                margin: 20
            }
        }
    });
    $( ".owl-prev").html('<img src="assets/images/icon_pre.svg"/>');
    $( ".owl-next").html('<img src="assets/images/icon_next.svg"/>');
})